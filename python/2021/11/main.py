import time

NS = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]
POSITIONS = set([(y, x) for x in range(10) for y in range(10)])


class Octopus:
    key: tuple = None
    energy: int = 0
    family: dict = None
    flashes: int = 0
    neighbors: list = None

    def __init__(self, key=(0, 0), energy=0, neighbors=None, family=None):
        self.key = key
        self.energy = energy
        self.family = family
        self.neighbors = neighbors

    def group(self):
        return [
            self.neighbor(*n) for n in self.neighbors
            if self.neighbor(*n).energy > 0
        ]

    def neighbor(self, y, x):
        return self.family[y, x]

    def increase(self):
        self.energy += 1
        if self.energy == 10:
            self.energy = 0
            return self
        return None


def parse_input(fp):
    inputs = []
    octos = {}
    for ln in fp:
        inputs.append([int(c) for c in ln.strip()])

    for y, x in POSITIONS:
        neighbors = [
            (y + _y, x + _x) for _y, _x in NS
            if 0 <= y + _y < 10 and 0 <= x + _x < 10
        ]
        octos[y, x] = Octopus(
            key=(y, x),
            energy=inputs[y][x],
            neighbors=neighbors,
            family=octos
        )

    return octos


def run(octos, condition):
    flashed = []
    flashes = 0
    step = 0
    while True:
        step_flash = 0
        for _, octo in octos.items():
            if octo.increase():
                flashed.append(octo)
                step_flash += 1

        while flashed:
            octo = flashed.pop()
            for n in octo.group():
                if n.increase():
                    flashed.append(n)
                    step_flash += 1

        flashes += step_flash

        step += 1

        if condition(step, step_flash, octos):
            break

    return flashes, step


def condition_one(step, *_):
    return step == 100


def condition_two(_, flashed, octos):
    return flashed == len(octos)


def main():
    with open('input.txt', 'r') as fp:
        octos = parse_input(fp)
        print(run(octos, condition_one))
    with open('input.txt', 'r') as fp:
        octos = parse_input(fp)
        print(run(octos, condition_two))


if __name__ == '__main__':
    start = time.time()
    main()
    end = time.time()
    tot = end - start
    print(f'Completed in {tot:.6f} seconds')
