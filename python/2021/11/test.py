import unittest

from main import run, parse_input, condition_two, condition_one

INPUT = [
    '5483143223',
    '2745854711',
    '5264556173',
    '6141336146',
    '6357385478',
    '4167524645',
    '2176841721',
    '6882881134',
    '4846848554',
    '5283751526',
]


class TestDay10(unittest.TestCase):
    def test_part_one_10_steps(self):
        def condition(step, *_):
            return step == 10
        expected = 204
        inputs = parse_input(INPUT)
        actual, _ = run(inputs, condition=condition)
        self.assertEqual(expected, actual)

    def test_part_one(self):
        expected = 1656
        inputs = parse_input(INPUT)
        actual, _ = run(inputs, condition=condition_one)
        self.assertEqual(expected, actual)

    def test_part_two(self):
        expected = 195
        inputs = parse_input(INPUT)
        _, actual = run(inputs, condition=condition_two)
        self.assertEqual(expected, actual)
